 # Attributon

Blankcanvas would not be possible if not for the hard work of programmers and designers. They have gracefully provided their work through gratice, to show gratitude we will provide all notable contributors here.

## Person's of interest

- Lawrence Turton (Avelx) main contributor, UI / UX / Architect
- Lissan UI / UX designer

## Artwork

- [Shards dashboard](https://designrevision.com/downloads/shards-dashboard-lite/) MIT License 
- [Dripicon](http://demo.amitjakhu.com/dripicons/) Attribution-ShareAlike 4.0 International
- [Tetrisly](https://tetrisly.com/) Creative Commons Attribution 4.0 International (CC BY 4.0) license