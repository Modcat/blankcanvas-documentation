- Basics

- [Getting Started](/)
- [Licensing](licensing.md)
- [Attribution](attribution.md)
- [OCSI - Open Creative Software Initiative](open_ideas.md)

- Workflow

- [Git Flow](git_flow.md)
- [Local File Flow](local_file_flow.md)
- [Data Flow](data_flow.md)
- [Render Flow](render_flow.md)
- [Network Flow](network_flow.md)
- [Pipeline Flow](pipeline_flow.md)
- [Server Migration Flow](server_migration_flow.md)

- Framework

- [UI / UX](ui_ux.md)
- [Flux](flux.md)
- [Initial Render](intial_render.md)
- [Filters](render_loop.md)
